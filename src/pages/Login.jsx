import React, { useContext } from "react";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebase";
import { ChatContext } from "../context/ChatContext";

const Login = () => {
  const [err, setErr] = useState(false);
  const navigate = useNavigate();
  const {dispatch} = useContext(ChatContext);
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = e.target[0].value;
    const password = e.target[1].value;

    dispatch(({type: "CHANGE_USER", payload: {}}));

    try {
      await signInWithEmailAndPassword(auth, email, password);
      navigate("/");
    } catch (err) {
      setErr(true);
      console.log(err);
    }
  };

  return (
    <div className="formContainer">
      <div className="formWrapper">
        <span className="logo">YLMY Chat</span>
        <span className="title">Login</span>
        <form onSubmit={handleSubmit}>
          <input type="email" placeholder="email" />
          <input type="password" placeholder="password" />
          <button>Sign in</button>
        </form>
        <span className="lrSwitch">Don't have an account? <Link to="/register">Register now!</Link></span>
        {err && <span>An error occur...</span>}
      </div>
    </div>
  );
};

export default Login;
