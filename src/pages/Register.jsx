import React, { useContext } from "react";
import { useState } from "react";
import Add from "../image/add_avatar1.png"; //LYS: why?
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { doc, setDoc } from "firebase/firestore";
import { auth, storage, db } from "../firebase";
import { useNavigate, Link } from "react-router-dom";
import { ChatContext } from "../context/ChatContext";

const Register = () => {
  const [errMessage, setErrMessage] = useState('');
  const navigate = useNavigate();
  const {dispatch} = useContext(ChatContext);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const userName = e.target[0].value;
    const email = e.target[1].value;
    const password = e.target[2].value;
    const file = e.target[3].files[0];

    if (!file) {
      alert("You haven't add your avatar yet! Please upload an image as your icon");
      return;
    }

    // make sure there's no chat context when register
    dispatch(({type: "CHANGE_USER", payload: {}}));

    try {
      const res = await createUserWithEmailAndPassword(auth, email, password); //LYS: this return userCredential, where userCredential.user is user

      const storageRef = ref(storage, userName); //name the image in the web storage. coule name it as the userName

      const uploadTask = uploadBytesResumable(storageRef, file);

      // Register three observers:
      // 1. 'state_changed' observer, called any time the state changes LYS: we delete this
      // 2. Error observer, called on failure
      // 3. Completion observer, called on successful completion
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");
        },
        (err) => {
          console.log(err.message);
          setErrMessage(err.message);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
            // return downloadURL for our avatar, complete user profile here (just add userName and photo, email and pwd been plougged when create)
            await updateProfile(res.user, {
              displayName: userName,
              photoURL: downloadURL,
            });
            // add the user into users database so other users can interact with this one
            await setDoc(doc(db, "users", res.user.uid), {
              //LYS: what does the id field inside doc() do - yeah, each documentation has an id
              uid: res.user.uid,
              userName,
              email,
              photoURL: downloadURL,
            });
            //create a document in userChats for this user, which will store all other users' info who has communicated with this user
            await setDoc(doc(db, "userChats", res.user.uid), {});
            //after successful registering, navigate to home page
            await setDoc(doc(db, "moments", res.user.uid), {
              momentuid: res.user.uid
            });

            navigate("/");
          });
        }
      );
    } catch (err) {
      console.log(err.code);
      if (err.code === 'auth/weak-password')
          setErrMessage('Password should be at least 6 characters');
      if (err.code === 'auth/email-already-in-use')
          setErrMessage('User Name or Email is already in use');
      if (err.code === 'auth/invalid-email')
          setErrMessage('Email is invalid');
    }
  };

  return (
    <div className="formContainer">
      <div className="formWrapper">
        <span className="logo">YLMY Chat</span>
        <span className="title">Register</span>
        <form onSubmit={handleSubmit}>
          <input type="text" placeholder="user name" />
          <input type="email" placeholder="email" />
          <input type="password" placeholder="password" />
          <input
            style={{ display: "none" }}
            type="file"
            id="avatar"
            placeholder="Add an icon"
          />
          <label htmlFor="avatar">
            <img src={Add} alt="" />
            <p>Add your avatar</p>
          </label>
          {errMessage && (<span className="errorMessage">{errMessage}</span>)}
          <button>Sign up</button>
        </form>
        <span className="lrSwitch">Already have an account? <Link to="/login">Login here!</Link></span>
        {/* {err && <span>An error occur</span>} */}
      </div>
    </div>
  );
};

export default Register;
