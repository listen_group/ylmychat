import React, { useContext } from 'react'
import Sidebar from '../components/Sidebar'
import Chat from '../components/Chat'
import { AuthContext } from '../context/AuthContext';

const Home = (props) => {

  // const { userState, setUserState } = props;
  // const {currentUser} = useContext(AuthContext);

  // if (currentUser)
  //   setUserState(userState + 1);


  return (
    <div className='Home'>
      <div className="container">
        <Sidebar />
        <Chat />
      </div>
    </div>
  )
}

export default Home