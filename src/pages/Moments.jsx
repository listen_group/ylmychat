import React from 'react'
import { useNavigate } from 'react-router-dom';


const Moments = (props) => {

    const { momentlist } = props;

    const navigate = useNavigate();

    const imagePopup = (image_src) => {
        window.open(image_src, "_blank");
    };


    return (
        <div className="Moments">
            <div className="Momentcontainer">
                <div className="Momentheader">
                    <i onClick={()=>navigate("/")} className="fa-solid fa-chevron-left"></i>
                    <p>Moments</p>
                    <i onClick={()=>navigate("/createMoment")} className="fa-regular fa-square-plus"></i>
                </div>
                <div className="MomentInfo">
                    {momentlist?.sort((a, b) => b[1].date - a[1].date).map((moment) => (
                        <div className="momentCollection" key={moment[0]}>
                            <div className="momentUser">
                                <img src={moment[1].momentInfo.photoURL} alt="" />
                            </div>
                            <div className="momentContent">
                                <p className="username">{moment[1].momentInfo.displayName}</p>
                                <span className="momentText">{moment[1].momentInfo.text}</span>
                                <img className="originimg" src={moment[1].momentInfo.picture} alt="" onClick={()=>imagePopup(moment[1].momentInfo.picture)} />
                                {/* <div className="largeimg">
                                    <img src={moment[1].momentInfo.picture} alt="" />
                                    <span><i className="fa-regular fa-circle-xmark"></i></span>
                                </div>  -- may for modal window view */}
                                <span className="momentTime">{moment[1].displaydate}</span>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Moments;
