import { doc, serverTimestamp, updateDoc } from 'firebase/firestore';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { storage, db } from '../firebase';
import Picture from "../image/addpicture.png"
import { useContext } from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { v4 as uuid } from "uuid";

const getCurrentTime = () => {
  var time = new Date();
  var Hours = time.getHours();
  if (Hours < 10) Hours = "0" + Hours;
  var Minutes = time.getMinutes();
  if (Minutes < 10) Minutes = "0" + Minutes;
  var Seconds = time.getSeconds();
  if (Seconds < 10) Seconds = "0" + Seconds;

  var datestring =
    time.getFullYear() +
    "-" +
    (time.getMonth() + 1) +
    "-" +
    time.getDate() +
    " " +
    Hours +
    ":" +
    Minutes +
    ":" +
    Seconds;

  return datestring;
};

const CreateMoment = (props) => {

    const { momentStatus ,setMomentStatus } = props;

    const navigate = useNavigate();
    const {currentUser} = useContext(AuthContext);
    const [textinput, setTextinput] = useState("");
    const [image, setImage] = useState(null);

    const handlePost = async () => {

        console.log(textinput)
        console.log(image)

        try {
            if (textinput === "" && image === null) {
                navigate("/moments");
                return
            }

            const storageRef = ref(storage, uuid());
            const uploadTask = uploadBytesResumable(storageRef, image);

            uploadTask.on(
                "state_changed",
                (snapshot) => {
                    // Observe state change events such as progress, pause, and resume
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    const progress =
                      (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log("Upload is " + progress + "% done");
                },
                (error) => {
                    console.log(error.message);
                }, 
                () => {
                    getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                        var momentid = uuid();
                        var datestring = getCurrentTime();
                        await updateDoc(doc(db, "moments", currentUser.uid), {
                            [momentid+".momentInfo"]: {
                                uid: currentUser.uid,
                                displayName: currentUser.displayName,
                                photoURL: currentUser.photoURL,
                                text: textinput,
                                picture: downloadURL,
                            },
                            [momentid+".date"]: serverTimestamp(),
                            [momentid+".displaydate"]: datestring
                        });
                        setTextinput("");
                        setImage(null);
                        setMomentStatus(momentStatus + 1);
                        navigate("/moments");

                    });
                }
            );
        } catch(err) {
            console.log(err.message);
        }
    }


    return (
        <div className="Moments">
            <div className="Momentcontainer post">
                <div className="Momentheader">
                    <i onClick={()=>navigate("/moments")} className="fa-solid fa-chevron-left"></i>
                    <p>Post your moments...</p>
                    <i onClick={handlePost} className="fa-regular fa-square-check"></i>
                </div>
                <input onChange={e=>setImage(e.target.files[0])} className="postpicture" type="file" id="file" />
                <div className="momentAddIcons">
                  <label className="addImage" htmlFor="file">
                    <img src={Picture} alt="" />
                    {image && <div className="redicon"></div>}
                  </label>
                </div>
                <input className="inputtext" type="text" placeholder="Type something here..." onChange={e=>setTextinput(e.target.value)} value={textinput} />
            </div>
        </div>
    )
}

export default CreateMoment;
