import React, { useState } from 'react'
import "./style.scss";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Home from "./pages/Home";
import { HashRouter, Routes, Route, Navigate } from "react-router-dom";
import {useContext} from "react";
import {AuthContext} from "./context/AuthContext";
import Moments from "./pages/Moments";
import CreateMoment from "./pages/CreateMoment";
import { collection, doc, getDoc, getDocs, query, where } from 'firebase/firestore';
import { useEffect } from "react";
import { db } from './firebase';


function App() {

  const {currentUser} = useContext(AuthContext);
  const ProtectedRoute = ({children})=>{
    if (!currentUser) {return <Navigate to="/login"/>}
    return children; //LYS: if there is a user, return Home page
  }
  // console.log(currentUser);

  const [momentStatus, setMomentStatus] = useState(0);
  const [momentlist, setMomentlist] = useState([]);

  const getfriendmoments = async () => {
    try {
        const resf = await getDoc(doc(db, "userChats", currentUser.uid));
        if (resf.exists()) {
            let newfriendlist = Object.entries(resf.data());
            let myfriendlist = [];
            newfriendlist.map((nf) => myfriendlist.push(nf[1].userInfo.uid));
            myfriendlist.push(currentUser.uid)

            let preMomentlist = [];
            const q = query(collection(db, "moments"), where("momentuid", "in", myfriendlist));
            const querySnapshot = await getDocs(q);
            if (querySnapshot._snapshot.docChanges.length !== 0) {
                querySnapshot.forEach((doc) => {
                    let addMoments =  Object.entries(doc.data());
                    addMoments = addMoments.filter(m => {return m[0] !== "momentuid"})
                    preMomentlist = preMomentlist.concat(addMoments);
                    setMomentlist(preMomentlist);
                });
            }
        }
    } catch(err) {
      console.log(err)
      // console.log(err.message)
    }
  }


  useEffect(() => {
    if (currentUser) {
      if(currentUser.uid !== undefined)
        getfriendmoments();
    }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentUser, momentStatus]);


  return (
    <HashRouter>
    {/* <AuthContextProvider> */}
      <Routes>
        <Route path="/">
          <Route index element={<ProtectedRoute><Home /></ProtectedRoute>}></Route>  {/* LYS: doesn't understand this */}
          {/* <Route index element={<Home />}></Route> */}
          <Route path="moments" element={<ProtectedRoute><Moments momentlist={momentlist} /></ProtectedRoute>}></Route>
          <Route path="createMoment" element={<ProtectedRoute><CreateMoment momentStatus={momentStatus} setMomentStatus={setMomentStatus} /></ProtectedRoute>}></Route>
          <Route path="login" element={<Login />}></Route>
          <Route path="register" element={<Register />}></Route>
        </Route>
      </Routes>
    {/* </AuthContextProvider> */}
    </HashRouter>


    // <BrowserRouter>
    //   <Routes>
    //     <Route path="/">
    //       <Route index element={
    //         <ProtectedRoute>
    //           <Home />
    //         </ProtectedRoute>
    //       } />
    //       <Route path="login" element={<Login />} />
    //       <Route path="register" element={<Register />} />
    //       <Route path="moments" element={<ProtectedRoute><Moments /></ProtectedRoute>}></Route>
    //       <Route path="createMoment" element={<ProtectedRoute><CreateMoment /></ProtectedRoute>}></Route>
    //     </Route>
    //   </Routes>
    // </BrowserRouter>
  );
}

export default App;
