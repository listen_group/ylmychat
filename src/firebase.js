import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDvKLdZlXkz2wgoX-lP6td3dDzII42btPo",
  authDomain: "chat-app-eba43.firebaseapp.com",
  projectId: "chat-app-eba43",
  storageBucket: "chat-app-eba43.appspot.com",
  messagingSenderId: "473511227345",
  appId: "1:473511227345:web:21c76e8568a45770d6faf0"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const storage = getStorage();
export const db = getFirestore();