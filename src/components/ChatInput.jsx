import React from "react";
import { IoImageOutline } from "react-icons/io5";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { useState } from "react";
import {
  arrayUnion,
  doc,
  increment,
  serverTimestamp,
  Timestamp,
  updateDoc,
} from "firebase/firestore";
import { db, storage } from "../firebase";
import { v4 as uuid } from "uuid";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

const ChatInput = () => {
  const { currentUser } = useContext(AuthContext);
  const { data } = useContext(ChatContext);
  const [text, setText] = useState("");
  const [image, setImage] = useState(null);
  const [err, setErr] = useState(false);

  const handleSend = async (e) => {
    e.preventDefault();
    if (!image && text === "") return;
    // if there is an image, send text and image; if not, just send text
    if (image) {
      const storageRef = ref(storage, uuid()); //name the image in the web storage. coule name it as the userName

      const uploadTask = uploadBytesResumable(storageRef, image);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");
        },
        (err) => {
          setErr(true);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
            // return downloadURL for our avatar, complete user profile here (just add userName and photo, email and pwd been plougged when create)
            await updateDoc(doc(db, "chats", data.chatId), {
              messages: arrayUnion({
                id: uuid(),
                text,
                senderId: currentUser.uid,
                date: Timestamp.now(),
                image: downloadURL,
              }),
            });
          });
        }
      );
    } else {
      await updateDoc(doc(db, "chats", data.chatId), {
        messages: arrayUnion({
          id: uuid(),
          text,
          senderId: currentUser.uid,
          date: Timestamp.now(),
        }),
      });
    }

    // upadate userChats for both sender and receiver
    // alert("start updating userChats doc1");
    await updateDoc(doc(db, "userChats", currentUser.uid), {
      [data.chatId + ".lastMessage"]: {
        text,
      },
      [data.chatId + ".date"]: serverTimestamp(),
      [data.chatId + ".numUnreads"]: 0,
    });
    // alert("finish updating userChats doc1");

    // alert("start updating userChats doc2");
    await updateDoc(doc(db, "userChats", data.user.uid), {
      [data.chatId + ".lastMessage"]: {
        text,
      },
      [data.chatId + ".date"]: serverTimestamp(),
      [data.chatId + ".numUnreads"]: increment(1),
    });
    // alert("finish updating userChats doc2");


    // after text (and image) been successfully uploaded to database, erase them
    setText("");
    setImage(null);
  };

  // This is for paste image in input box
  window.addEventListener('paste', e => {
    const fileInput = document.getElementById("document_attachment_doc");
    // alert("paste happen here")
    // const dataTransfer = new DataTransfer();
    // const myFile = e.clipboardData.files[0];
    // dataTransfer.items.add(myFile);
    fileInput.files = e.clipboardData.files; //LYS: unsolved! why this doesn't trigger the onChange event of the file-type input?
    setImage(fileInput.files[0])
    // console.log(fileInput)
  });

  return (
    <div className="chatInput">
      <form onSubmit={handleSend}>
        <input
          type="text"
          placeholder="Type something..."
          onChange={(e) => {
            setText(e.target.value);
          }}
          value={text}
        />
        <div className="send">
          <div className="chatInputIcons">
            <input
              style={{ display: "none" }}
              type="file"
              accept="image/*"
              id="document_attachment_doc"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
            />
            <label htmlFor="document_attachment_doc" className="addImage">
              <IoImageOutline style={{ color: "grey", fontSize: "1.5em" }} />
              {image && <div className="redicon"></div>}
              {/* <div className="redicontext">
                <span className="todotext">1</span>
              </div> */}
            </label>
          </div>
          <button onClick={handleSend}>Send</button>
        </div>
      </form>
    </div>
  );
};

export default ChatInput;
