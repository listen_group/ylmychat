import React from 'react'
import { BsThreeDots } from "react-icons/bs";
import { ChatContext } from '../context/ChatContext'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom';

const ChatHeader = () => {
  const {data} = useContext(ChatContext);
  const navigate = useNavigate();

  const popupMenu = ()=>{
    // alert("Sorry, we haven't implement menu functionality yet")
    navigate("/moments");
  }

  return (
    <div className='chatHeader'>
        <span>{data.user?.userName}</span>
        <div onClick={popupMenu} className="icons">
          <span>Moments</span>
          {/* <BsThreeDots style={{color: "white", fontSize: "1.5em"}} onClick={popupMenu}/> */}
          <i className="fa-solid fa-icons"></i>
        </div>
    </div>
  )
}

export default ChatHeader
