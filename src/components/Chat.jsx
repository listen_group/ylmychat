import React from 'react'
import Messages from './Messages'
import ChatInput from './ChatInput'
import ChatHeader from './ChatHeader'
import ChatGuide from './ChatGuide'
import { ChatContext } from '../context/ChatContext'
import { useContext } from 'react'


const Chat = () => {
  const {data} = useContext(ChatContext);


  if (data.chatId==='null') {
    return(
    <div className='chat'>
      <ChatGuide />
    </div>
  )}
 
  else
  return (
    <div className='chat'>
      <ChatHeader />
      <Messages />
      <ChatInput />
    </div>
  )
}

export default Chat