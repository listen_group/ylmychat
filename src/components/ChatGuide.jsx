import React from 'react'
import ChatHeader from './ChatHeader'

const ChatGuide = () => {
  return (
    <div className='chatGuide'>
      <ChatHeader />
      <div className="welcomeText">
          <span>Welcome to YLMY Chat!</span>
          <span>Search for a user, then begin chatting</span>
          <span>Enjoy!</span>
      </div>
      {/* <div className="inviteText">
          <span>No Friends using this?</span>
          <span>Don't know who to search?</span>
          <span>Come chat with me! User name: YLMY</span>
          <span>Won't be online too often{")"} Recommand this to your friends!</span>
      </div> */}
    </div>
  )
}

export default ChatGuide