import React, { useContext } from "react";
import { useState } from "react";
import {
  collection,
  query,
  where,
  getDocs,
  getDoc,
  setDoc,
  doc,
  updateDoc,
  serverTimestamp,
} from "firebase/firestore";
import { db } from "../firebase";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";

const Search = () => {
  const [userName, setUserName] = useState(""); //the name we are searching
  const [user, setUser] = useState(null); //the user we are chatting with
  const [userNotFound, setUserNotFound] = useState(false);
  const [err, setErr] = useState(false);
  const { currentUser } = useContext(AuthContext); //add the curly braces!!!
  const { dispatch, data } = useContext(ChatContext);

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Create a reference to the users collection
    const usersRef = collection(db, "users");

    // Create a query against the collection.
    const q = query(usersRef, where("userName", "==", userName));

    try {
      const querySnapshot = await getDocs(q);
      if (querySnapshot.empty) {
        setUser(null);
        setUserNotFound(true);
      }
      querySnapshot.forEach((doc) => {
        // console.log(doc.data());
        setUser(doc.data());
      });
    } catch (err) {
      setErr(true);
    }
  };

  const addUserInChat = async () => {
    //check whether this user already in chat with owner. if not, create a new doc in "chats" collection between owner and this user
    const jointId =
      currentUser.uid > user.uid
        ? currentUser.uid + user.uid
        : user.uid + currentUser.uid;
    try {
      const res = await getDoc(doc(db, "chats", jointId));
      //!res.exists()
      if (!res.exists()) {
        //create a chat in "chats" collection
        await setDoc(doc(db, "chats", jointId), { messages: [] });

        //create userChats
        await updateDoc(doc(db, "userChats", currentUser.uid), {
          [jointId + ".userInfo"]: {
            uid: user.uid,
            userName: user.userName,
            photoURL: user.photoURL,
          },
          [jointId + ".date"]: serverTimestamp(),
          [jointId + ".numUnreads"]: 0,
        });
        await updateDoc(doc(db, "userChats", user.uid), {
          [jointId + ".userInfo"]: {
            uid: currentUser.uid,
            userName: currentUser.displayName,
            photoURL: currentUser.photoURL,
          },
          [jointId + ".date"]: serverTimestamp(),
          [jointId + ".numUnreads"]: 0,
        });
      }
    } catch (err) {
      alert("there's an error. see console");
      console.log(err);
      setErr(true);
    }

    dispatch({ type: "CHANGE_USER", payload: user });

    setUser(null);
    setUserName("");
  };

  return (
    <div className="search">
      <form action="" className="searchForm" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Type to search"
          className="searchInput"
          onChange={(e) => {
            setUserName(e.target.value);
            setUserNotFound(false);
          }}
          value={userName}
        />
      </form>
      {!(userName === "") && userNotFound && (
        <span style={{ padding: "10px", color: "rgb(175, 175, 175)" }}>
          user not found..
        </span>
      )}
      {user && (
        <div className="userChat" onClick={addUserInChat}>
          <img src={user.photoURL} alt="" />
          <div className="userChatInfo">
            <span>{user.userName}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default Search;
