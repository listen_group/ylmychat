import React from 'react'
import Message from './Message'
import { ChatContext } from '../context/ChatContext'
import { useContext } from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { doc, onSnapshot } from 'firebase/firestore'
import {db} from "../firebase"

const Messages = () => {
  const {data} = useContext(ChatContext);
  const [messages, setMessages] = useState([]);
  const [lastMessage, setLastMessage] = useState({});


  useEffect(()=>{
    const unsub = onSnapshot(doc(db, "chats", data.chatId), (doc)=>{
      doc.exists() && setMessages(doc.data().messages);
    })

    return ()=>{
      unsub();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.chatId]);

  return (
    <div className='messages'>
      {messages.map((m, index, messages)=>(
        <Message message={m} prevMessage={index===0 ? null:messages[index-1]} key={m.id}/>
      ))}
    </div>
  )
}

export default Messages