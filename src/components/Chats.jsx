import React, { useState, useContext } from "react";
import { useEffect } from "react";
import { doc, increment, onSnapshot, updateDoc } from "firebase/firestore";
import { db } from "../firebase";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";

const Chats = () => {
  const [chats, setChats] = useState([]);
  const { currentUser } = useContext(AuthContext);
  const { data, dispatch } = useContext(ChatContext);
  const oneDayInMilliSec = 86400000;

  useEffect(() => {
    const getChats = () => {
      const unsub = onSnapshot(doc(db, "userChats", currentUser.uid), (doc) => {
        setChats(doc.data());
        // console.log(chats);  // LYS: strange. chats here seem to be empty, but we somehow get the correct behavior. And, if we insert in a "jointId as null" doc, page crashed.
        // console.log(doc.data());
      });
      return () => {
        unsub(); // LYS: why unsub: the section above return is the behavior we do when compunents mount, inside return is the action when component unmount. Perform same actions when unmount, avoid memory leak
      };
    };

    currentUser.uid && getChats();
  }, [currentUser.uid]); // not sure about the dependencies here

  const handleSelect = async (chat1) => {
    dispatch({ type: "CHANGE_USER", payload: chat1.userInfo });

    //set numUnreads to 0
    const jointId =
      currentUser.uid > chat1.userInfo.uid
        ? currentUser.uid + chat1.userInfo.uid
        : chat1.userInfo.uid + currentUser.uid;
    await updateDoc(doc(db, "userChats", currentUser.uid), {
      [jointId + ".numUnreads"]: 0, 
    });
  };

  return (
    <div className="chats">
      {/* LYS: why's the ? for below? BTW Object.entries(obj) convert object into array */}
      {Object.entries(chats)
        ?.sort((a, b) => b[1].date - a[1].date)
        .map((chat) => (
          <div
            className={`userChat ${
              (data.user) && (chat[1].userInfo) && (chat[1].userInfo.uid === data.user.uid) && "selectedUser"
            }`}
            key={chat[0]}
            onClick={() => handleSelect(chat[1])}
          >
            <img src={chat[1].userInfo.photoURL} alt="" />
            <div className="MissedMessage">
              {/* <img src={chat[1].userInfo.photoURL} alt="" /> */}
              {(!(chat[1].numUnreads===0)) && (<div className="redicon"><div className="rediconText">
                <span>{(chat[1].numUnreads>99)? "99+" : chat[1].numUnreads}</span>
              </div></div>)}
            </div>

            <div className="userChatInfo">
              <span>{chat[1].userInfo.userName}</span>
              {chat[1].lastMessage && (
                <p>
                  {chat[1].lastMessage?.text
                    ? chat[1].lastMessage?.text
                    : "[image]"}
                </p>
              )}
            </div>
            {chat[1].date && (
              <p className="messageTime">
                {new Date().toDateString() ===
                new Date(chat[1].date.seconds * 1000).toDateString()
                  ? new Date(chat[1].date.seconds * 1000).toLocaleTimeString()
                  : new Date(chat[1].date.seconds * 1000).toLocaleDateString()}
              </p>
            )}
          </div>
        ))}
      {/* notice the pink brace, remember, ()=>() return the component in the second braces */}
    </div>
  );
};

export default Chats;
