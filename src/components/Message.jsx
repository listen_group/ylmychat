import React, { useEffect, useRef, useState } from "react";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { useNavigate, Link } from "react-router-dom";

const Message = ({ message, prevMessage }) => {
  const { currentUser } = useContext(AuthContext);
  const { data } = useContext(ChatContext);
  const ref = useRef();
  const [minDiff, setMinDiff] = useState(0); // minDiff means the minutes difference between cur message and last message


  function getMinuteDiff(startDate, endDate) {
    return Math.round(
      Math.abs(endDate - startDate) / 60
    );
  }
  

  

  useEffect(() => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
    if (prevMessage) setMinDiff(getMinuteDiff(prevMessage.date, message.date));
    else setMinDiff(0);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [message]);

  const imagePopup = () => {
    window.open(message.image, "_blank");
  };

  if (!(message.text === "") || message.image)
    return (
      <div
        ref={ref}
        className={`message ${message.senderId === currentUser.uid && "owner"}`}
      >
        {(minDiff>=2) && <div className="messageStatus">
          <div><p>{new Date().toDateString() ===
                new Date(message.date.seconds * 1000).toDateString()
                  ? new Date(message.date.seconds * 1000).toLocaleTimeString()
                  : new Date(message.date.seconds * 1000).toLocaleString()}</p></div>
        </div>}

        <div className="belowMessageStatus">
          <div className="messageInfo">
            <img
              src={
                message.senderId === currentUser.uid
                  ? currentUser.photoURL
                  : data.user.photoURL
              }
              alt=""
            />
            {/* <span>just now</span> */}
          </div>

          <div className="messageContent">
            {!(message.text === "") && <p>{message.text}</p>}
            {message.image && (
              <img src={message.image} alt="" onClick={imagePopup} />
            )}
          </div>
        </div>
      </div>
    );
};

export default Message;