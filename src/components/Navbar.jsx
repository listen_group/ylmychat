import { signOut } from 'firebase/auth'
import React from 'react'
import { auth } from '../firebase'
import {useContext} from "react";
import {AuthContext, AuthContextProvider} from "../context/AuthContext";

const Navbar = () => {
  const {currentUser} = useContext(AuthContext);

  return (
    <div className='navbar'>
        <span className="logo">YLMY Chat</span>
        <div className="userInfo">
            <img src={currentUser.photoURL} alt="" className="userIcon" />
            <span>{currentUser.displayName}</span>
            <button onClick={()=>signOut(auth)}>logout</button>
        </div>
    </div>
  )
}

export default Navbar;