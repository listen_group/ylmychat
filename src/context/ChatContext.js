//LYS: this file is for the context of the current chatting user
import { onAuthStateChanged } from "firebase/auth";
import { useContext } from "react";
import { createContext, useReducer } from "react";
import { useState, useEffect } from "react";
import { auth } from "../firebase";
import {AuthContext} from "./AuthContext"

export const ChatContext = createContext();

export const ChatContextProvider = ({children}) => {
  const {currentUser} = useContext(AuthContext);
  const INIT_STATE = {
    chatId : "null",
    user: {}
  }
  
  const chatReducer = (state, action)=> {
    switch (action.type) {
      case "CHANGE_USER":
        return {
          user: action.payload,
          // chatId: (currentUser.uid > action.payload.uid ? currentUser.uid+action.payload.uid : action.payload.uid+currentUser.uid),
          chatId: !action.payload.uid ? "null" : (currentUser.uid > action.payload.uid ? currentUser.uid+action.payload.uid : action.payload.uid+currentUser.uid),
        }
      
      default: return state;
    }
  };

  const [state, dispatch] = useReducer(chatReducer, INIT_STATE);

  return(
    <ChatContext.Provider value={{data: state, dispatch}}> 
        {children} 
    </ChatContext.Provider>
  );
};
